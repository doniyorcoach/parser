const { PDFDocument } = require("pdf-lib");
const fs = require("fs").promises;
const path = require("path");

const findRstFiles = async (dirName) => {
  const rstFilesArray = [];

  const scanDir = async (dir) => {
    const files = await fs.readdir(dir);

    for (const file of files) {
      const filePath = path.join(dir, file);
      const stat = await fs.stat(filePath);

      if (stat.isFile() && path.extname(filePath) === ".rst") {
        rstFilesArray.push(filePath);
      } else if (stat.isDirectory()) {
        await scanDir(filePath);
      }
    }
  };

  try {
    await scanDir(dirName);
    return rstFilesArray;
  } catch (error) {
    console.error("Ошибка при сортировке RST файлов ", error);
  }
};

const findPdfFiles = async (dirName) => {
  const pdfFiles = [];

  const files = await fs.readdir(dirName);
  files.forEach((file) => {
    const filePath = path.join(dirName, file);
    pdfFiles.push(filePath);
  });

  return pdfFiles;
};

const getDate = () => {
  const currentDate = new Date();

  return currentDate;
};

const appendPdfFiles = async (pdfPaths) => {
  try {
    const pdfDoc = await PDFDocument.create();

    for (const pdfPath of pdfPaths) {
      const pdfBytes = await fs.readFile(pdfPath);
      const pdf = await PDFDocument.load(pdfBytes);
      const pages = await pdfDoc.copyPages(pdf, pdf.getPageIndices());

      pages.forEach((page) => {
        pdfDoc.addPage(page);
      });
    }

    const pdfBytes = await pdfDoc.save();
    await fs.writeFile(`documentations/${getDate()}.pdf`, pdfBytes);
  } catch (error) {
    console.error("Ошибка при сборке PDF файла", error);
  }
};

module.exports = { findRstFiles, appendPdfFiles, findPdfFiles, getDate };
