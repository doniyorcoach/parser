const cron = require("node-cron");
const {
  fetchDocumentation,
  deleteUnneededFolders,
  convertToPdf,
} = require("./console");
const { findRstFiles, findPdfFiles, appendPdfFiles } = require("./handlers");

const start = async () => {
  console.log("Клонирование репозитория ...");
  await fetchDocumentation();
  1;
  console.log("Сортировка .rst файлов...");
  const rstFiles = await findRstFiles("documentation");

  console.log("Конвертация .rst в .pdf ...");
  const conversionPromises = rstFiles.map(async (filePath, index) => {
    await convertToPdf(filePath, index);
  });
  await Promise.all(conversionPromises);

  console.log("Сортировка .pdf файлов...");
  const pdfFiles = await findPdfFiles("mock");

  console.log("Объединение .pdf файлов...");
  await appendPdfFiles(pdfFiles);

  console.log("Удаленые ненужных файлов...");
  await deleteUnneededFolders();

  console.log("\nУспешно documentation.pdf!");
};

// set cron
const task = cron.schedule("*/4 * * * *", () => {
  start();
});

task.start();

// const task = cron.schedule("0 0 * * 1", () => {
//   start();
// });

// task.start();
