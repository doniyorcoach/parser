const util = require("util");
const exec = util.promisify(require("child_process").exec);

const fetchDocumentation = async () => {
  try {
    await exec(
      "git clone https://github.com/nextcloud/documentation.git manual"
    );
    await exec("mv manual/developer_manual documentation");
    await exec("rm -rf manual");
  } catch (error) {
    console.error("Ошибка при клонирование репозитория:", error);
  }
};

const deleteUnneededFolders = async () => {
  try {
    await exec("rm -rf mock documentation");
    await exec("mkdir mock");
  } catch (error) {
    console.error("Ошибка при удалении директории:", error);
  }
};

const convertToPdf = async (rstPath, pdfName) => {
  try {
    await exec(`rst2pdf ${rstPath} mock/${pdfName}.pdf`);
  } catch (error) {
    // получаем предупреждение, нельзя конвертировать svg( на работу не влияет)
  }
};

module.exports = {
  fetchDocumentation,
  deleteUnneededFolders,
  convertToPdf,
};
